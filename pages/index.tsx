import Head from "next/head";
import Layout from "@/components/Layout";

export default function Home() {
  return (
    <>
      <Head>
        <title>Dobu Technology</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta property="og:title" content="Dobu Technology"></meta>
        <meta property="og:type" content="website"></meta>
        <meta property="og:url" content="https://www.dobu.io/"></meta>
        <meta property="og:site_name" content="Dobu Technology"></meta>
        <meta property="og:image" content="/static/thumbnailLogo.png" />

        <meta
          property="og:description"
          content="Today's trend is our yesterday. We deliver the future!"
        ></meta>
      </Head>

      <main className="flex flex-col w-full h-full items-center justify-center ">
        <Layout />
      </main>
    </>
  );
}
