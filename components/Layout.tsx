import Header from "./PageComponents/Header";
import MainPage from "./PageComponents/MainPage";
import SystemCore from "./PageComponents/SystemCore";
import ControlSystem from "./PageComponents/ControlSystem";
import EntertainmentSystem from "./PageComponents/EntertainmentSystem";
import GateSurveillance from "./PageComponents/GateSurveillance";
import LinkAccess from "./PageComponents/LinksAccess";
import FireAlarm from "./PageComponents/FireAlarm";
import IntrusionAlarm from "./PageComponents/IntrusionAlarm";
import AirQuality from "./PageComponents/AirQuality";
import HeatingCooling from "./PageComponents/HeatingCooling";
import Footer from "./PageComponents/Footer";

const Layout = () => {
  return (
    <div className="flex flex-col max-w-[1366px] h-max items-center justify-center px-[43px] ">
      <div className="flex sticky top-0 w-full z-[50] ">
        <Header />
      </div>
      <div className="w-full overflow-y-scroll">
        <MainPage />
        <SystemCore />
        <ControlSystem />
        <EntertainmentSystem />
        <GateSurveillance />
        <LinkAccess />
        <FireAlarm />
        <IntrusionAlarm />
        <AirQuality />
        <HeatingCooling />
        <Footer />
      </div>
    </div>
  );
};

export default Layout;
