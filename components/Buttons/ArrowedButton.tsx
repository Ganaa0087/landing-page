import React, { FC, useState } from "react";
import Image from "next/image";
import ArrowButton from "../../assets/Images/ArrowButton.svg";
import ArrowButtonBlack from "../../assets/Images/ArrowButtonBlack.svg";

interface Props {
  text: string;
}

const ArrowedButton: FC<Props> = (props) => {
  const [isHovering, setIsHovered] = useState(false);
  const onMouseEnter = () => setIsHovered(true);
  const onMouseLeave = () => setIsHovered(false);
  return (
    <div
      onMouseEnter={onMouseEnter}
      onMouseLeave={onMouseLeave}
      className="w-max text-[16px] text-[#ED5727] hover:text-[#000000] flex flex-row items-center gap-[8px] hover:gap-[16px]"
    >
      <div className="flex">{props.text}</div>
      <div className="flex">
        {isHovering ? (
          <Image
            src={ArrowButtonBlack}
            width={"16"}
            alt={"arrow"}
            height={"16"}
          />
        ) : (
          <Image src={ArrowButton} width={"16"} alt={"arrow"} height={"16"} />
        )}
      </div>
    </div>
  );
};

export default ArrowedButton;
