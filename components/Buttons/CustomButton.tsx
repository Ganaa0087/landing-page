import React from "react";

const CustomButton = () => {
  return (
    <button className="text-[16px] w-[160px] h-[40px] text-[#ED5727]  border-[#ED5727] hover:border-[#000000] hover:text-[#000000] border-4 border-opacity-80">
      Learn More
    </button>
  );
};
export default CustomButton;
