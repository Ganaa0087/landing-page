import Link from "next/link";
import Image from "next/image";
import React from "react";
import Youtube from "../../assets/Images/Youtube.svg";
import Linkedin from "../../assets/Images/LinkedIn.svg";
import Instagram from "../../assets/Images/Instagram.svg";
import Facebook from "../../assets/Images/Facebook.svg";

const Footer = () => {
  return (
    <div className="w-full h-max flex flex-row pt-[73px] pb-[20px] ">
      <div className="w-1/4  flex flex-col h-full items-start pr-[30px]">
        <div className="flex flex-col w-full h-max">
          <div className="flex flex-row w-full h-max">
            <div className="flex w-1/4 h-[24px]">
              <Link
                key={"https://www.linkedin.com/company/dobu-technology/"}
                href={"https://www.linkedin.com/company/dobu-technology/"}
                target="_blank"
              >
                <Image
                  src={Linkedin}
                  alt="Linkedin logo"
                  width={24}
                  height={24}
                />
              </Link>
            </div>
            <div className="flex w-1/4 h-[24px]">
              <Link
                key={"https://www.youtube.com/@dobuofficial"}
                href={"https://www.youtube.com/@dobuofficial"}
                target="_blank"
              >
                <Image
                  src={Youtube}
                  alt="Youtube logo"
                  width={24}
                  height={24}
                />
              </Link>
            </div>
            <div className="flex w-1/4 h-[24px]">
              <Link
                key={"https://www.instagram.com/dobu_official/"}
                href={"https://www.instagram.com/dobu_official/"}
                target="_blank"
              >
                <Image
                  src={Instagram}
                  alt="Instagram logo"
                  width={24}
                  height={24}
                />
              </Link>
            </div>
            <div className="flex w-1/4 h-[24px]">
              <Link
                key={"https://www.facebook.com/dobu.official"}
                href={"https://www.facebook.com/dobu.official"}
                target="_blank"
              >
                <Image
                  src={Facebook}
                  alt="Facebook logo"
                  width={24}
                  height={24}
                />
              </Link>
            </div>
          </div>
          <div className="flex w-full text-[14px] text-[#000000] fira-sans opacity-75 pt-[24px] font-FiraSansLight">
            Copyright © 2023 Dobu. All Rights Reserved.
          </div>
        </div>
      </div>
      <div className="w-1/4 flex flex-col h-full items-start pr-[50px]">
        <div className="flex w-full h-max mb-[8px] text-[16px] leading-tight font-[600]">
          DOBU HMS
        </div>
        <div className="flex w-full h-max text-[14px] hover:underline pb-[2px] text-[#000000] fira-sans opacity-75 ">
          What is Dobu House Management System?
        </div>
        <div className="flex w-full h-max text-[14px] hover:underline pb-[2px] text-[#000000] fira-sans opacity-75 ">
          Healthy House
        </div>
        <div className="flex w-full h-max text-[14px] hover:underline pb-[2px] text-[#000000] fira-sans opacity-75 ">
          Net Zero Energy House
        </div>
        <div className="flex w-full h-max text-[14px] hover:underline pb-[2px] text-[#000000] fira-sans opacity-75 ">
          Secure and Safe House
        </div>

        <div className="flex w-full h-max mb-[8px] mt-[16px] text-[16px] leading-tight font-[600]">
          Events
        </div>
        <div className="flex w-full h-max text-[14px] hover:underline pb-[2px] text-[#000000] fira-sans opacity-75 ">
          Exhibitions
        </div>
        <div className="flex w-full h-max text-[14px] hover:underline pb-[2px] text-[#000000] fira-sans opacity-75 ">
          Webinars
        </div>
        <div className="flex w-full h-max text-[14px] hover:underline pb-[2px] text-[#000000] fira-sans opacity-75 ">
          Podcasts
        </div>
      </div>
      <div className="w-1/4 flex flex-col h-full items-start  pr-[50px]">
        <div className="flex w-full h-max mb-[8px] text-[16px] leading-tight font-[600]">
          DOBU SYSTEMS AND SOLUTIONS
        </div>
        <div className="flex w-full h-max text-[14px] hover:underline pb-[2px] text-[#000000] fira-sans opacity-75 ">
          Dobu House Management System Core
        </div>
        <div className="flex w-full h-max text-[14px] hover:underline pb-[2px] text-[#000000] fira-sans opacity-75 ">
          Dobu Automation and Control
        </div>
        <div className="flex w-full h-max text-[14px] hover:underline pb-[2px] text-[#000000] fira-sans opacity-75 ">
          Dobu House Entertainment System
        </div>
        <div className="flex w-full h-max text-[14px] hover:underline pb-[2px] text-[#000000] fira-sans opacity-75 ">
          Dobu Smart Gate Control System
        </div>
        <div className="flex w-full h-max text-[14px] hover:underline pb-[2px] text-[#000000] fira-sans opacity-75 ">
          Video Surveillance System
        </div>
        <div className="flex w-full h-max text-[14px] hover:underline pb-[2px] text-[#000000] fira-sans opacity-75 ">
          Dobu Fire Alarm System Lite
        </div>
        <div className="flex w-full h-max text-[14px] hover:underline pb-[2px] text-[#000000] fira-sans opacity-75 ">
          Dobu Intrusion Alarm System
        </div>
        <div className="flex w-full h-max text-[14px] hover:underline pb-[2px] text-[#000000] fira-sans opacity-75 ">
          Dobu Smart Ventilation and Air Quality Monitoring System
        </div>
        <div className="flex w-full h-max text-[14px] hover:underline pb-[2px] text-[#000000] fira-sans opacity-75 ">
          Dobu Smart Heating and Cooling System
        </div>
      </div>
      <div className="w-1/4 flex flex-col h-full items-start  pr-[50px]">
        <div className="flex w-full h-max mb-[8px] text-[16px] leading-tight font-[600]">
          About Us
        </div>
        <div className="flex w-full h-max text-[14px]  hover:underline pb-[2px] text-[#000000] fira-sans opacity-75 ">
          Our Company
        </div>
        <div className="flex w-full h-max text-[14px]  hover:underline pb-[2px] text-[#000000] fira-sans opacity-75 ">
          Careers
        </div>
        <div className="flex w-full h-max text-[14px] hover:underline pb-[2px] text-[#000000] fira-sans opacity-75 ">
          News
        </div>
        <div className="flex w-full h-max text-[14px] hover:underline pb-[2px] text-[#000000] fira-sans opacity-75 ">
          Locations
        </div>
        <div className="flex w-full h-max text-[14px] hover:underline pb-[2px] text-[#000000] fira-sans opacity-75 ">
          Contact Us
        </div>

        <div className="flex w-full h-max mb-[8px] mt-[16px] text-[16px] leading-tight font-[600]">
          SERVICE AND SUPPORT
        </div>
        <div className="flex w-full h-max text-[14px] hover:underline pb-[2px] text-[#000000] fira-sans opacity-75 ">
          Frequently Asked Questions
        </div>
        <div className="flex w-full h-max text-[14px]  hover:underline pb-[2px] text-[#000000] fira-sans opacity-75 ">
          Service and Support
        </div>
      </div>
    </div>
  );
};

export default Footer;
