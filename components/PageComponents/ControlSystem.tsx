import PageWithTwoColumn from "../PageTemplates/PageWithTwoColumn";
import React from "react";
import controlSystem2x from "../../assets/Images/controlSystem2x.png";

const ControlSystem = () => {
  return (
    <PageWithTwoColumn
      header="KEY TO THE SMART HOME"
      title="Dobu Automation and Control System"
      text1="An intelligent home is vital to comfortable and convenient living. Whether it is lights, curtains, electricity, or heating, Dobu Automation and Control System manages all. At whatever time, wherever you are, controls of your house are at your fingertips, letting you stay informed and in control. "
      text2="Dobu Automation and Control System recognizes patterns and predicts your usage from turning lights off when you are not there to opening curtains when you wake up. Everyday tasks are automated using our behavioral analysis algorithm, allowing you to spend time with more important things that matter for you."
      image={controlSystem2x}
    />
  );
};

export default ControlSystem;
