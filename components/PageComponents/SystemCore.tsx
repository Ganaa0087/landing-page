import PageWithTwoColumn from "../PageTemplates/PageWithTwoColumn";
import React from "react";
import Mask_Group5 from "../../assets/Images/Mask_Group5.png";

const SystemCore = () => {
  return (
    <PageWithTwoColumn
      header="HOUSE MANAGEMENT SYSTEM HEART AND BRAIN"
      title=" Dobu House Management System Core"
      text1="Dobu House Management System is powered by its core - the heart and brain of your smart home. Dobu HMS Core collects and stores data and ensures seamless and reliable operation of all systems. From ventilation to heating, security to lighting, Dobu HMS Core intelligently orchestrates the coordination between systems to provide you with a seamless and fully integrated home experience. "
      text2="Dobu HMS Core is your house's data center, server, network infrastructure, and the bridge between you and your home. On the state-of-the-art, technologically advanced hardware, Dobu HMS Core runs Dobu GER OS, an operating system with entirely new and immensely complex algorithms and Al technologies that deliver exceptional performance and reliability to fulfill all of your smart home needs."
      //     text3="In addition to the set of hardware, we also developed an operating
      // system with entirely new and immensely complex algorithms and AI
      // technologies to handle such extensive duties reliably and efficiently."
      image={Mask_Group5}
    />
  );
};

export default SystemCore;
