import PageWithOneColumn from "../PageTemplates/PageWithOneColumn";
import React from "react";
import Cooling2x from "../../assets/Images/Cooling2x.png";

const HeatingCooling = () => {
  return (
    <PageWithOneColumn
      header="YOUR HEALTH IS OUR PRIORITY"
      title=" Dobu Smart Heating and Cooling System"
      text1="Dobu Smart Heating and Cooling System is the cornerstone of net zero energy living solutions while offering unparalleled comfort and energy efficiency. Our state-of-the-art heating and cooling system is a comprehensive and complex engineering solution with the best possible harmony between energy sources and numerous environmental sensors. At its foundation, we developed a complex algorithm based on the laws of thermodynamics, and we embedded the algorithm in Dobu GER OS."
      text2="Say goodbye to high energy bills and hello to a more sustainable lifestyle. Dobu Smart Heating and Cooling System can reduce your energy consumption by up to 5 times when it dynamically works with the ventilation system. Overall, Dobu HMS reliably and efficiently works to save every possible cent for you 24/7 without your involvement. So, please feel the fully automated, control-free, technologically advanced house."
      buttonText="Learn More About Heating & Cooling System"
      image={Cooling2x}
    />
  );
};

export default HeatingCooling;
