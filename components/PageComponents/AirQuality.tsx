import PageWithOneColumn from "../PageTemplates/PageWithOneColumn";
import React from "react";
import Monitoring2x from "../../assets/Images/Monitoring2x.png";

const AirQuality = () => {
  return (
    <PageWithOneColumn
      header="YOUR HEALTH IS OUR PRIORITY"
      title="Dobu Smart Ventilation and Air Quality Monitoring System"
      text1="Dobu Smart Ventilation and Air Quality Monitoring System is the pinnacle of modern home and building management. With its seamless integration with other systems, Dobu Smart Ventilation system provides a smooth and energy-efficient experience for fresh air circulation. "
      text2="Our advanced air quality monitoring system constantly monitors the air quality in each room through array of sensors, and Dobu HMS system acts accordingly to ensure clean and fresh air for your home. The system utilizes Energy Recovery Ventilation (ERV) with a top-of-the-line HEPA filter to deliver the purest air while conserving up to 85% energy. With the Dobu Smart Ventilation and Air Quality Monitoring System, you can breathe easy knowing your air is always top-quality."
      buttonText="Learn More About Ventilation System"
      image={Monitoring2x}
    />
  );
};

export default AirQuality;
