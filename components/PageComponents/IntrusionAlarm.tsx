import PageWithTwoColumn from "../PageTemplates/PageWithTwoColumn";
import React from "react";
import Intrusion2x from "../../assets/Images/Intrusion2x.png";
import Engineering2x from "../../assets/Images/Engineering@2x.png";

const IntrusionAlarm = () => {
  return (
    <PageWithTwoColumn
      header="LIVE FREE OF DANGER"
      title="Dobu Intrusion Alarm System"
      text1="Stay protected with Dobu's state-of-the-art Intrusion Alarm System. This innovative system offers a user experience unlike any other, with features such as intelligent arming and disarming, self-monitoring against system malfunctioning, and real-time detection of break-ins. "
      text2="Anytime, anywhere, our intrusion alarm system detects break-ins in real-time and lets you respond to the incursions before it gets too late. The system can identify the precise location quickly and immediately notify you of the threat. Since it is part of a more extensive, intelligently orchestrated house management system, other systems also take possible necessary actions against the burglar. Stay ahead of the game with Dobu Intrusion Alarm System."
      image={Intrusion2x}
      image2={Engineering2x}
    />
  );
};

export default IntrusionAlarm;
