import PageWithTwoColumn from "../PageTemplates/PageWithTwoColumn";
import React from "react";
import LinkAccess2x from "../../assets/Images/LinkAccess2x.png";
import LivingRoom2x from "../../assets/Images/LivingRoom2x.png";

const LinkAccess = () => {
  return (
    <PageWithTwoColumn
      header="NEVER MISS YOUR GUEST AT YOUR FRONT DOOR"
      title=" Dobu Links and Access System"
      text1="Dobu Links and Access System is a reliable guard, a universal key, and a digital reception to your entrance doors. Anytime, anywhere you are, you are able to monitor and access your front door in real-time. In addition, by the meaning of links, you can video call your guest through our innovative IP-based door stations and smart bells."
      text2="As of tailored systems design and engineering solutions company, we experienced a lot of complaints about manual keys to access doors. It seems easy, but it isn't very nice when you are in charge of a few entries with traditional keys. So let's make your mobile phone a universal key to all your entries. How does this sound to you?"
      image={LinkAccess2x}
      image2={LivingRoom2x}
    />
  );
};

export default LinkAccess;
