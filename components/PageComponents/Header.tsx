import React, { useState } from "react";
import Image from "next/image";
import Dobu_Header from "../../assets/Images/Dobu_Header_Logo@2x.png";
import Mask_Group from "../../assets/Images/Mask_Group.png";
import SearchIcon from "../../assets/Images/Search.svg";
import SearchIconWhite from "../../assets/Images/SearchWhite.svg";
import MenuIcon from "../../assets/Images/Menu.svg";
import MenuIconWhite from "../../assets/Images/MenuWhite.svg";
import { useRouter } from "next/router";
const DobuLogo = () => {
  const router = useRouter();
  return (
    <Image
      src={Dobu_Header}
      alt="Dobu logo"
      layout="responsive"
      onClick={() => router.reload()}
    />
  );
};

const Header = () => {
  const [isHoveringSearch, setIsHoveredSearch] = useState(false);
  const onMouseEnterSearch = () => setIsHoveredSearch(true);
  const onMouseLeaveSearch = () => setIsHoveredSearch(false);
  const [isHovering, setIsHovered] = useState(false);
  const onMouseEnter = () => setIsHovered(true);
  const onMouseLeave = () => setIsHovered(false);
  return (
    <div className="flex w-full flex-row justify-between bg-white py-[16px]">
      <div className="w-[40px] h-[40px]">
        <DobuLogo />
      </div>
      <div className="h-max flex flex-row justify-center ">
        <div className="w-[320px] h-[40px] border-[1px]  border-[#020202] mr-[30px] items-center flex justify-center  text-[#000000] font-extralight hover:bg-black hover:text-white">
          Get Started
        </div>
        <div
          onMouseEnter={onMouseEnterSearch}
          onMouseLeave={onMouseLeaveSearch}
          className="w-[40px] h-[40px] items-center flex justify-center border-[1px]  border-[#020202] hover:bg-black hover:text-white hover:bg-black hover:text-white"
        >
          {isHoveringSearch ? (
            <div className="flex justify-center">
              <Image src={SearchIconWhite} alt="Dobu logo" />
            </div>
          ) : (
            <div className="flex justify-center">
              <Image src={SearchIcon} alt="Dobu logo" />
            </div>
          )}
        </div>
        <div
          onMouseEnter={onMouseEnter}
          onMouseLeave={onMouseLeave}
          className="w-[40px] h-[40px] items-center flex justify-center border-y-[1px] border-r-[1px]   border-[#020202]  hover:bg-black hover:text-white"
        >
          {isHovering ? (
            <div className="flex justify-center">
              <Image src={MenuIconWhite} alt="menu" />
            </div>
          ) : (
            <div className="flex justify-center">
              <Image src={MenuIcon} alt="menu" />
            </div>
          )}
        </div>
      </div>
    </div>
  );
};
export default Header;
