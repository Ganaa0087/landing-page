import React, { useState } from "react";
import Image from "next/image";
import CoverPhoto from "../../assets/Images/Cover.png";

const MainPage = () => {
  return (
    <div className="flex flex-col w-full h-full py-[30px] border-b border-gray-200">
      <div className="flex flex-col mt-[30px] text-[40px] font-bold leading-tight">
        <div> {"Today's trend is our"}</div>
        <div>yesterday.</div>
        <div>We deliver the future!</div>
      </div>
      <div className="w-full flex flex-1 pt-[16px]">
        <Image src={CoverPhoto} alt="Cover" />
      </div>
      <div className="flex flex-col mt-[32px] h-[max]">
        <div className="text-[30px] leading-tight font-[600]">
          One-stop solution for every imaginable technology needs for your house
          while enabling healthy, comfortable, and net zero energy living!
        </div>
        <div className="text-[14px] fira-sans  text-[#000000] opacity-75 pt-[8px]  mt-[5px]">
          We developed Dobu House Management System (HMS), an innovative,
          comprehensive, and complex engineering solutions consisting of Dobu
          HMS Core—Dobu GER OS operating system running on the state-of-the-art
          hardware, automation and control systems, entertainment system, smart
          gate control system, advanced video surveillance, links and access
          systems, fire prevention and intrusion alarm systems, on-demand energy
          recovery ventilation system, and an efficient heating and cooling
          systems. Imagine that these systems are part of the broader integrated
          management system and can operate simultaneously and intelligently in
          an orchestrated manner.
        </div>
        <div className="text-[14px] text-[#000000] fira-sans opacity-75 mt-[10px]">
          Dobu HSM is your proven way to achieve carbon-free and renewable
          energy lifestyle. With Dobu, you can trust in a proven solution for a
          healthier, hassle-free, and safer home environment.
        </div>
      </div>
    </div>
  );
};

export default MainPage;
