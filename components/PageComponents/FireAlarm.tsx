import PageWithOneColumn from "../PageTemplates/PageWithOneColumn";
import React from "react";
import FireAlarm2x from "../../assets/Images/FireAlarm2x.png";

const FireAlarm = () => {
  return (
    <PageWithOneColumn
      header="LIVE FREE OF DANGER"
      title="Dobu Fire Alarm System Lite"
      text1="To meet tomorrow's requirements, we must update and re-design aged technologies, like fire alarms and burglar alarm systems. Yet, we are still stuck with old bulky metal boxes with LEDs and battery-powered unreliable single detectors with annoying and false alarms. "
      text2="Our cutting-edge addressable fire alarm system offers an innovative user experience and never-seen-before features, including self-monitoring against system malfunctioning and real-time notification on your mobile devices. Moreover, Dobu Fire Alarm System Lite is a cost-efficient and reliable fire alarm system to date."
      buttonText="Learn More About Fire Alarm System"
      image={FireAlarm2x}
    />
  );
};

export default FireAlarm;
