import PageWithOneColumn from "../PageTemplates/PageWithOneColumn";
import React from "react";
import HouseEntertainment2x from "../../assets/Images/HouseEntertainment2x.png";

const EntertainmentSystem = () => {
  return (
    <PageWithOneColumn
      header="INVENT YOUR SOUNDSCAPE"
      title=" Dobu House Entertainment System"
      text1="Dobu House Entertainment System is the solution to create your perfect housing soundscape. With one press of a button, you are able to fully immerse yourself in whatever sound you choose — at high quality. Furthermore, Dobu HEM quickly delivers the most suitable environment for your mood by orchestrating other connected systems. 
              "
      text2="From separating each room’s sound to combining speakers and bringing the different parts of your house together, high-quality speakers deliver this with customizable modes. So whether you want a movie night surround sound or some ambient bedtime music for your child, Dobu HEM is the key."
      buttonText="Learn More About Entertainment System"
      image={HouseEntertainment2x}
    />
  );
};

export default EntertainmentSystem;
