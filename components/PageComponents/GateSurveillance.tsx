import PageWithTextOnly from "../PageTemplates/PageWithTextOnly";
import React from "react";

const GateSurveillance = () => {
  return (
    <div className="flex flex-row h-max w-full border-t border-b border-black pb-[32px]">
      <PageWithTextOnly
        title=" Dobu Smart Gate Control System"
        text1="Welcome home in style with the Dobu Smart Gate Control System. Say goodbye to manual gate operations and enjoy a seamless, intelligent entrance to your property. With advanced cameras utilizing License Plate Recognition and Facial Recognition technologies, our system knows exactly who's at the gate and triggers a warm welcome by coordinating with other systems. 
              "
        text2="Sit back, relax, and let the systems take care of the rest. The Dobu Smart Gate Control System is designed to provide you and your guests with a hassle-free, secure, and personalized entrance experience every time you come home and your guests pay a visit."
        buttonText="Learn More About Gate Control System"
      />
      <PageWithTextOnly
        title=" Video Surveillance System"
        text1="Feel secure in your own home with our advanced surveillance system. Protect your property with ease using cameras based on ONVIF, the gold standard in IP-based digital cameras. Our expertly designed and intuitive system ensures that video surveillance is simple, effective, and easy to use. 
              "
        text2="Stay connected and in control with the ability to view every part of your home from your mobile device in high quality. Your satisfaction and peace of mind are our top priorities, which is why we design and tailor our system to meet your specific needs and requirements."
        buttonText="Learn More About Surveillance System"
      />
    </div>
  );
};

export default GateSurveillance;
