import { title } from "process";
import React, { FC } from "react";
import ArrowedButton from "../Buttons/ArrowedButton";

interface Props {
  title: string;
  text1: string;
  text2: string;
  buttonText: string;
}

const PageWithTextOnly: FC<Props> = (props) => {
  return (
    <div className="flex flex-col justify-between  w-[50%] h-full pr-[25px] ">
      <div className="flex w-full flex-col h-full ">
        <div className="text-[24px]  leading-tight font-[600] mt-[30px]">
          {props.title}
        </div>
        <div className="text-[14px]  text-[#000000] fira-sans opacity-75 py-[15px]">
          {props.text1}
        </div>
        <div className="text-[14px]  text-[#000000] fira-sans opacity-75 pb-[20px]">
          {props.text2}
        </div>
      </div>

      <ArrowedButton text={props.buttonText} />
    </div>
  );
};

export default PageWithTextOnly;
