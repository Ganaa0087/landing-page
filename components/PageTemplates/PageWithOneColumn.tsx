import React, { FC } from "react";
import Image, { StaticImageData } from "next/image";
import ArrowedButton from "../Buttons/ArrowedButton";

interface Props {
  header: string;
  title: string;
  image: StaticImageData;
  text1: string;
  text2: string;
  buttonText: string;
}

const PageWithOneColumn: FC<Props> = (props) => {
  return (
    <div className="flex flex-row h-max w-full border-b border-gray-200 py-[24px]">
      <div className="w-full">
        <div className="text-[24px] text-[#000000] font-[100]">
          {props.header}
        </div>
        <div className="text-[24px] leading-tight font-[600] mt-[30px]">
          {props.title}
        </div>
        <div className="text-[14px] text-[#000000] fira-sans opacity-75 py-[15px]">
          {props.text1}
        </div>
        <div className="text-[14px] text-[#000000] fira-sans opacity-75 pb-[20px]">
          {props.text2}
        </div>
        <ArrowedButton text={props.buttonText} />
        <div className="w-full mt-[16px]">
          <Image src={props.image} alt="image" />
        </div>
      </div>
    </div>
  );
};

export default PageWithOneColumn;
