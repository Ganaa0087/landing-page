import React, { FC } from "react";
import CustomButton from "../Buttons/CustomButton";
import Image, { StaticImageData } from "next/image";

interface Props {
  header: string;
  title: string;
  image: StaticImageData;
  text1: string;
  text2: string;
  text3?: string;
  image2?: StaticImageData;
}

const PageWithTwoColumn: FC<Props> = (props) => {
  return (
    <div className="flex flex-col h-max w-full  border-b border-gray-200 pt-[32px] ">
      <div className="flex flex-row h-max w-full  pb-[32px]">
        <div className="w-[50%]  pr-[25px]">
          <div className="text-[24px] text-[#000000] font-[100]">
            {props.header}
          </div>
          <div className="text-[24px]  leading-tight font-[600] mt-[24px]">
            {props.title}
          </div>
          <div className="text-[14px] text-[#000000] fira-sans opacity-75 py-[20px]">
            {props.text1}
          </div>
          <div className="text-[14px] text-[#000000] fira-sans opacity-75 pb-[20px]">
            {props.text2}
          </div>
          {props.text3 ? (
            <div className="text-[14px] text-[#000000] fira-sans opacity-75 pb-[20px]">
              {props.text3}
            </div>
          ) : (
            ""
          )}
          <CustomButton />
        </div>
        <div className="w-[50%] relative">
          <Image src={props.image} alt="image" layout="fill" />
        </div>
      </div>
      {props.image2 ? (
        <div className="w-full">
          <Image src={props.image2} alt="image" />
        </div>
      ) : (
        ""
      )}
    </div>
  );
};

export default PageWithTwoColumn;
